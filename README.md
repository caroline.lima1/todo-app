# Todo List

## Features
- React
- Node.js
- Axios
- MongoDB
- Mongoose
- Cypress
- Jest
- KISS and DRY principles and Mediator design pattern

## Usage 

### Start
- Clone this repo to your desktop and run npm install to install all the dependencies
- Once the dependencies are installed, at client folder you can run npm run start to start the application and access it at localhost:3000. At server folder you can run npm start run server and access it at localhost:5000/todos

### Tests
- Jest is used for unit and integration tests. Execute npm test at server folder to run the tests
- Cypress is used to end-to-end tests. Execute at client folder npm start run cypress

