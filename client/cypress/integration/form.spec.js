describe('Form', () => {
    beforeEach(() => {
      cy.visit('/')
    })
  
    it('it focuses the input', () => {
      cy.focused().should('have.class', 'todo-input')
    })

    it('accepts input', () => {
      const input = "Learn about Cypress"
      cy.get('.todo-input')
        .type(input)
        .should('have.value', input)
    })

    it('button add new task', () => {
      cy.get('.todo-button').click({ force: true })
    })

  })
