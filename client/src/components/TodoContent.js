import React, { useEffect, useState } from 'react';
import axios from "axios";
import TodoForm from './TodoForm';
import TodoList from './TodoList';

export default class TodoContent extends React.Component{

  state = {
    todos: [],
  }
  componentDidMount() {
    axios.get('/todos')
      .then(async res => {
        const todos = res.data;
        await this.setState({ todos });
      })
    };

  addTodo = async todo => {
    if (!todo.title || /^\s*$/.test(todo.title)) {
      return;
    }
    const newTodo = [{...todo}];
    await axios.post('/todos', ...newTodo )
    .then(res => {
      console.log(res);
      console.log(res.data);
      window.location.reload()
    })
  };
    render() {
      return (
        <>
      <h1>What's the Plan for Today?</h1>
      <TodoForm onSubmit={this.addTodo} />
      <TodoList
        todos={this.state.todos.map(todo => todo.title)}
      />
    </>
    )
  }
}