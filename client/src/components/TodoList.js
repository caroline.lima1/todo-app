import React, { useState } from 'react';
import axios from "axios";
// import TodoForm from './TodoForm';
import { RiCloseCircleLine } from 'react-icons/ri';

export default class TodoList extends React.Component{
state = {
  todos: [],
};

componentDidMount() {
  axios.get('/todos')
    .then(async res => {
      const todos = res.data;
      await this.setState({ todos });
    })
  };

  async completeTodo(id){
    let updatedTodo = await this.state.todos.filter(todo => todo._id === id)
    axios.put(`/todos/${id}`, 
    {
      "title": updatedTodo[0].title,
      "done": !updatedTodo[0].done,
    })
    .then(res => {
      console.log(res);
      console.log(res.data);
      window.location.reload()
    })
  } 

  removeTodo(id) {
    axios.delete(`/todos/${id}`) 
    .then(res => {
      console.log(res);
      console.log(res.data);
      window.location.reload()
    })
  }
  render() {
  return this.state.todos.map((todo, index) => (
    <div
      className={todo.done ? 'todo-row complete' : 'todo-row'}
      key={index}
    >
      <div key={todo._id} onClick={() => this.completeTodo(todo._id)}>
        {todo.title}
      </div>
      <div className='icons'>
        <RiCloseCircleLine
          key={todo._id} onClick={() => this.removeTodo(todo._id)}
          className='delete-icon'
        />
      </div>
    </div>
  ));
  }
};