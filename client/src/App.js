import React from 'react';
import './App.css';
import TodoContent from './components/TodoContent';

export default class App extends React.Component{
  render() {
    return (
      <div className='todo-app'>
        <TodoContent />
      </div>
    );
  }
}
